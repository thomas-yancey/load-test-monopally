const puppeteer = require('puppeteer');

const imageName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);



function delay(time) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, time)
    });
 }

(async () => {
    const browser = await puppeteer.launch({
        devtools: false,
        headless: true
    });
    const page = await browser.newPage();
    await page.setViewport({
        width: 360,
        height: 600,
        deviceScaleFactor: 1
    })
    await page.goto('http://nyc.growyourfortune.com/players/new?property=cc_nyc');

    await page.evaluate(() => {
        document.getElementById('new_player_submit').click()
    });


    var myPage = page
    await delay(100)

    await myPage.type('#email', 'tomyncy618@gmail.com', {delay: 1})
    await myPage.type('#email-confirmation', 'tomyncy618@gmail.com', {delay: 1})

    await delay(100)
    await page.evaluate(() => { document.getElementById('new_player_submit').click() });


    await delay(100)
    await page.evaluate(() => { 
        document.querySelector('#tos-checkbox').click() 
    });
    await page.evaluate(() => {
        document.querySelector('#player-form').submit() 
    })

    await page.waitForNavigation();
    console.log('awaited navigation')

    await page.screenshot({
        path: `${imageName}.png`
    })
    console.log('screenshiot taken')

    await browser.close();

})();
